 param ( [string]$profile )

function main() {
  param(
    [string]$computer = “localhost”
  )
  $is_laptop = Get-WmiObject -Class win32_battery -ComputerName $computer
  if($is_laptop)
  {
    #check if cisco vpn .msi file exists
    if([System.IO.File]::Exists("anyconnect-win-4.6.02074-core-vpn-predeploy-k9.msi"))
    {
      Start-Process -FilePath msiexec.exe -ArgumentList "/passive /i anyconnect-win-4.6.02074-core-vpn-predeploy-k9.msi /lex c:\options\logs\anyconnect.log" -Wait
      Write-Host "Cisco vpn installation completed."
      if([System.IO.File]::Exists($profile)) { Copy-Item $profile -Destination "C:\ProgramData\Cisco\Cisco AnyConnect Secure Mobility Client\Profile\" -force }
      else { Write-Host "No profile imported. Copy xml file for the profile to this project directory" (Get-Location) }
    }
    else { Write-Host "File - anyconnect-win-4.6.02074-core-vpn-predeploy-k9.msi missing! Please copy/download the msi file to this project directory " (Get-Location) }
    }
  else { Write-Host "This computer is not a laptop. Hence, not installing cisco vpn on this system."
  }
}


mkdir c:\options\logs -ErrorAction SilentlyContinue
main
