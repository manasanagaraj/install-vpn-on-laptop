# Install VPN on Laptop

This script installs cisco vpn client on a system if it is a laptop. It checks the battery status to determine if a system is laptop. Copy vpn installation msi file and the profile to be imported to the project directory before executing the script. Also, edit the script to change the msi file name based on the version of the client you are installing. (This will be automated in the future)

To run the script, .\install_vpn.ps1 -profile <profile.xml>
